
svSite
-----------------------------

**svSite is under development, there is no stable release at this time.**

svSite is a project to create a website that may be used by many small- to medium sized association. It is created with university study associations in mind, but may be extended beyond that.

Leonardo, a Dutch study association, like many other such groups, needed a website. However, experiences with general purpose CMS packages were mixed at best beyond the simplest of tasks. Having a custom website built was unaffordable for a modest association of students, and would honestly have been wasteful even had the money been available. It was decided to create a new website from scratch. And to make the code freely available for other associations, such that very similar projects need not be created over and over by slightly different groups.

svSite aims to be just general enough to be usable by most small- or medium sizes associations, without being overly complicated in order to be completely general purpose. The aim is for it to be usable without altering any code. However, many associations will want to use a custom look, which necessarily requires HTML and CSS. Except for appearance, no modifications are required, though contributions are certainly welcome!

Functionality
-----------------------------

[will be written later]

Installation
-----------------------------

At the moment, you must host your own copy of svSite. There are several requirements:

* Django: svSite is made in Django
* Python 3: Django is made in Python, svSite uses Python 3 (not 2)
* Elasticsearch: for search functionality
* Virtualenv is recommended, like it is for most projects, but not strictly required. These steps assume virtualenv is used.

To get svSite running on an Ubuntu server, including the above requirements, follow these steps:

1. Install the requirements. *If elasticsearch doesn't install, have a look at [elasticsearch repositories](http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/setup-repositories.html)*.
```bash
sudo apt-get install git python3 python3-pip python-virtualenv elasticsearch memcached
```
2. Create and enter a virtual environment
```bash
virtualenv -q -p /usr/bin/python3.4 env
. env/bin/activate
```
3. Clone this repository (in the directory you want the code to be, e.g. /live/):
```bash
	git clone https://mverleg@bitbucket.org/mverleg/svsite.git svsite
```
4. Install pip requirements (no sudo)
```bash
pip install -r dev/pip.freeze
```


It is a future goal to allow simple hosting for svSite communities, but this milestone has not been reached yet.

How to use it
-----------------------------

[will be written later]

How to change it
-----------------------------

[will be written later]

License
-----------

Available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use any contributor's name for promotion and are aware that there is no warranty.


