
"""
	Note that machine-specific or secret settings should go in local.py .
"""

from django.conf.global_settings import *
from os import path
BASE_DIR = path.dirname(path.dirname(__file__))


"""
	Project properties.
"""
SITE_URL = 'svsite.nl'
ALLOWED_HOSTS = ['.markv.nl', '.svsite.nl']
ROOT_URLCONF = 'urls'
WSGI_APPLICATION = 'wsgi.application'
STATIC_URL = '/static/'


"""
	Components.
"""
INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


"""
	Internationalization.
"""
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


"""
	Import local settings (override general settings).
"""
from local import *


