
"""
	Machine dependent or secret settings, not included in the repository.
"""

from os import path

BASE_DIR = path.dirname(path.dirname(__file__))

SECRET_KEY = '_d9+%7sro9k(i8@ga2*&oj$!_f=&)^x92stozwt=s%kbvyv09('

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.sqlite3',
		'NAME': path.join(BASE_DIR, 'data/default.sqlite3'),
	}
}

DEBUG = True
TEMPLATE_DEBUG = DEBUG


